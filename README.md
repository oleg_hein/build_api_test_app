A simple API-Test App.

# Whats missing for NXT Starter Project:

+ SASS sourcemapping
+ SVGO Loader
+ REACT-intl
+ Autoprefixer Settings
+ React-Motion

# Whats inside

+ React
+ React-Router-DOM
+ Typescript+TSX
+ Jest
+ MobX
+ Superagent
+ Webpack
    + Autoprefixer
    + Ugly-/Minifying